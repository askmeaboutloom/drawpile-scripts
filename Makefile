all: docs/drawpile-host.md docs/drawpile-check.md docs/drawpile-glue.md

docs/drawpile-host.md: drawpile-host
	pod2markdown <$< >$@

docs/drawpile-check.md: drawpile-check
	pod2markdown <$< >$@

docs/drawpile-glue.md: drawpile-glue
	pod2markdown <$< >$@

.PHONY: all
