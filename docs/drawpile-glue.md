# NAME

drawpile-glue - glue together `drawpile-check` and `drawpile-host`

# VERSION

version 0.02\_1

# SYNOPSIS

    drawpile-check --user|-u USERNAME
                  [--password|-p] PASSWORD
                  [--host-password|-q] HOST-PASSWORD
                  [--address|-a] ADDRESS
                  [--port|-d] PORT
                  [--verbose|-v] / [--no-verbose|-nov]
                  [CONFIG-FILE...]

# OPTIONS

The options given to this script will be passed through to
`drawpile-check` and `drawpile-glue`. See their documentation about
how the parameters affect them.

# CONFIGURATION

After all options are interpreted, the remaining arguments given are
opened in sequence and each one is read. If no config files are given,
the configuration will be read from stdin.

The config file is line-based and structured as follows:

    # anything after `#` is considered a comment
    # width height title
      2000  2000   Awesome Canvas
    # optional: a `--` followed by extra drawpile-host arguments
      3000  3000   Another Canvas -- -n notes-file

Any canvases that don't exist on the server you're checking will be
created using `drawpile-host`. Only their name matters, this script
doesn't check if the resolution of the existing canvas is correct or
whatever, since that may change during its lifespan.

# BUGS

Probably inherited from the scripts it calls.

# AUTHOR

askmeaboutloom <askmeaboutloom@live.de>

# COPYRIGHT

This software is public domain, do what you want with it.
