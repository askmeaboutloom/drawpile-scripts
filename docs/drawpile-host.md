# NAME

drawpile-host - tell a drawpile server to create a canvas

# VERSION

version 0.02\_1

# SYNOPSIS

    drawpile-host --user|-u USERNAME
                 [--password|-p] PASSWORD
                 [--host-password|-q] HOST-PASSWORD
                 [--address|-a] ADDRESS
                 [--port|-d] PORT
                 [--title|-t] CANVAS-TITLE
                 [--max-users|-m] MAX-USERS
                 [--lock-layer-control|-l] / [--no-lock-layer-control|-nol]
                 [--persistence|-r] / [--no-persistence|-nor]
                 [--preserve-chat|-c] / [--no-preserve-chat|-noc]
                 [--width|-x] CANVAS-WIDTH
                 [--height|-y] CANVAS-HEIGHT
                 [--notes|-n] NOTES-FILE
                 [--verbose|-v] / [--no-verbose|-nov]
                 [--help|-h]

# OPTIONS

- `--user=USERNAME, -u USERNAME`

    Set the username to log into the drawpile server with. This parameter
    is required. Note that Drawpile seems to allow the same user to log in
    multiple times, so you don't have to worry about collisions.

- `--password=PASSWORD, -p PASSWORD`

    The password for the given user. Can be left out if there's no password
    on the user you want to use or if you only need a hosting password.
    Defaults to no password.

- `--host-password=HOST-PASSWORD, -q HOST-PASSWORD`

    The hosting password for the server. Can be left out if the hosting
    password on the server isn't set. Defaults to no hosting password.

- `--address=ADDRESS, -a ADDRESS`

    The address of the Drawpile server you want to connect to.

    Defaults to `localhost`.

- `--port=PORT, -d PORT`

    Port of the Drawpile server you want to connect to.

    Defaults to `27750`.

- `--title=CANVAS-TITLE, -t CANVAS-TITLE`

    The name for the canvas you're going to create.

    Defaults to `Canvas`.

- `--max-users=MAX-USERS, -m MAX-USERS`

    Set the maximum number of users that can connect to the canvas.

    Defaults to 20.

- `--lock-layer-control, -l / --no-lock-layer-control, -nol`

    Lock or unlock the layer controls for regular users.

    Defaults to layer controls being free for everyone to use.

- `--persistence, -r / --no-persistence, -nor`

    Enable or disable persistence on the canvas. Note that persistence
    needs to be enabled on the Drawpile server if you want to use this.

    Also note that this program will disconnect right after creating the
    canvas, so it's probably kinda useless to disable persistence. But
    whatever, I'm not judging your life's decisions.

    Defaults to persistence enabled, obviously.

- `--preserve-chat, -c / --no-preserve-chat, -noc`

    Enable or disable persistent chat. Without this, chat won't replay for
    people who connect later. I think. To be honest, I always have this on.

    Defaults to preserving chat.

- `--width=WIDTH, -x WIDTH`

    The width for your canvas. Defaults to 2000.

- `--height=HEIGHT, -y HEIGHT`

    The height for your canvas. Defaults to 2000.

- `--verbose, -v / --no-verbose, -nov`

    Be noisy about messages read and written. The information will be
    written to STDERR.

- `--notes NOTES-FILE, -n NOTES-FILE`

    Load notes from the specified `NOTES-FILE` and add them to the canvas.
    This is useful to, say, add rules to the created canvas You can also
    use `-` as the file, which will read them from stdin.

    The notes file is structured as follows:

        # x-position y-position width height BGRA-color (reverse ARGB)
          -200       0          150   100    ffffffff
        # Note content in HTML, no blank lines
        <h1>RULES</h1>
        <ul>
            <li>no running</li>
            <li>no smoking</li>
            <li>no loitering</li>
        </ul>
        
        # next note after a blank line

    Anything after # is considered a comment and will be replaced with a
    single space.

- `--help, -h`

    Show this here help.

# BUGS

Probably. This is hacked together by intercepting the communication
that happens between server and client when a canvas is hosted, and
there's no error handling to speak of. It seems to work fine for me
though.

# TODO

In decreasing order of likelyhood that they're gonna happen.

- Allow layer options when creating canvas, such as how many layers are
to be created, what they're supposed to be called and what their
background is like.
- Also allow layers to be locked, because locking the background is really
useful.
- Allow loading of existing images instead of just blank canvases.
- Make this code nice.

# AUTHOR

askmeaboutloom <askmeaboutloom@live.de>

# COPYRIGHT

This software is public domain, do what you want with it.
