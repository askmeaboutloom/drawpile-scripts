# NAME

drawpile-check - list canvases running on a drawpile server

# VERSION

version 0.02

# SYNOPSIS

    drawpile-check --user|-u USERNAME
                  [--password|-p] PASSWORD
                  [--host-password|-q] HOST-PASSWORD
                  [--address|-a] ADDRESS
                  [--port|-d] PORT
                  [--format|-f] FORMAT
                  [--verbose|-v] / [--no-verbose|-nov]
                  [--help|-h]

# OPTIONS

- `--user=USERNAME, -u USERNAME`

    Set the username to log into the drawpile server with. This parameter
    is required. Note that Drawpile seems to allow the same user to log in
    multiple times, so you don't have to worry about collisions.

- `--password=PASSWORD, -p PASSWORD`

    The password for the given user. Can be left out if there's no password
    on the user you want to use or if you only need a hosting password.
    Defaults to no password.

- `--host-password=HOST-PASSWORD, -q HOST-PASSWORD`

    The hosting password for the server. Can be left out if the hosting
    password on the server isn't set. Defaults to no hosting password.

- `--address=ADDRESS, -a ADDRESS`

    The address of the Drawpile server you want to connect to.

    Defaults to `localhost`.

- `--port=PORT, -d PORT`

    Port of the Drawpile server you want to connect to.

    Defaults to `27750`.

- `--format=FORMAT, -d FORMAT`

    The format of the output. This will be interpolated as a Perl string
    and printed for each canvas. Please escape your `"`, otherwise you
    might get compile errors.

    The following variables are available:

    - `$title` - the title of the canvas
    - `$users` - the number of users currently connected to the canvas
    - `$creator` - the name of the user who hosted the canvas

    The default format is `$title\n`, i.e. it'll print the title of each
    canvas and a newline. If you want all the info, you could use
    `$title\t$users\t$creator\n` for example.

    **Note:** this string will be `eval`ed. No attempts are made to
    sanitize it. I don't know what would ride you to ever do this, but
    don't use a format string supplied by an external user - they can eval
    arbitrary Perl code with it.

- `--verbose, -v / --no-verbose, -nov`

    Be noisy about messages read and written. The information will be
    written to STDERR.

- `--help, -h`

    Show this here help.

# BUGS

Probably. This is hacked together from intercepting the network
communication. Weird usernames and such might break the parsing.

# AUTHOR

askmeaboutloom <askmeaboutloom@live.de>

# COPYRIGHT

This software is public domain, do what you want with it.
