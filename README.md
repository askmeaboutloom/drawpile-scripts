# drawpile-scripts

These are scripts to mess with dedicated
[Drawpile](http://drawpile.net/) servers.

They should run out-of-the-box on any Unix-like system. For Windows,
you'll need to install [perl](http://strawberryperl.com/) first.

Documentation for how the scripts work can be obtained by calling them
with the `--help` option or by reading the `.md` file of the same name
in the [docs](docs) folder.


## drawpile-host

Connect to a Drawpile server and make it host canvases.


## drawpile-check

List canvases already running on a Drawpile server.


## drawpile-glue

Glue together [drawpile-host](drawpile-host) and
[drawpile-check](drawpile-check): read which canvases are running on a
Drawpile server and create canvases that are missing.
